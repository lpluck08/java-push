package com.klaus.javapush;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.devices.Device;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.AppleNotificationServer;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.PushedNotifications;
import javapns.notification.transmission.NotificationThread;

public class AppPushServer {
	private static List<String>			tokens				= new ArrayList<String>();
//	private static String				certificatePath		= "/Users/corptest/Project/aps/enterprise_aps_development.p12";
//	private static String				certificatePath		= "/Users/corptest/Project/aps/enterprise_aps_production.p12";
//	private static String				certificatePath		= "/Users/corptest/Project/aps/company_aps_development.p12";
	private static String				certificatePath		= "/Users/corptest/Project/aps/company_aps_production.p12";
	private static String				certificatePassword	= "111111";
	private static NotificationThread	queuethread;																		// queue模式的线程。
	private static int					queuecount			= 0;

	// private static final boolean production = false;
	private static final boolean		production			= true;

	public static void main(String[] args) throws Exception {
		push();
		feedback();
	}

	private static void push() throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("uri", "sd/re");
		AppPushServer aps = new AppPushServer();
		aps.addTokenDevice("f2a80701604567959e743b0088e9c07a2b074baa57f89a6a4954a00f74334336");
//		aps.addTokenDevice("e71a9446d0e126dd3b0e148a101b59ef869c391018620499acce459b76eeb14e");
		aps.pushMessage("66666", "66", "default", data);
	}

	private static void feedback() throws CommunicationException,
			KeystoreException {
		List<Device> devices = Push.feedback(certificatePath,
				certificatePassword, production);
		for (Device basicDevice : devices) {
			System.out.println(basicDevice.getToken());
			System.out.println(basicDevice.getDeviceId());
		}
	}

	// 将queue模式的线程初始化，并启动它。之后有新的device加入，就会发送推送消息。
	static {
		AppleNotificationServer server;
		try {
			server = new AppleNotificationServerBasicImpl(certificatePath,
					certificatePassword, production);
			System.out.println("applehost = "
					+ server.getNotificationServerHost());
			System.out.println("appleport = "
					+ server.getNotificationServerPort());
			PushNotificationManager manager = new PushNotificationManager();
			manager.initializeConnection(server);
			queuethread = new NotificationThread(manager, server);
			System.out.println("queuethread max conn = "
					+ queuethread.getMaxNotificationsPerConnection());
			queuethread.start();
		} catch (KeystoreException e) {
			e.printStackTrace();
		} catch (CommunicationException e) {
			e.printStackTrace();
		}

	}

	// 保存设备的token
	public boolean addTokenDevice(String deviceToken) {
		System.out.println("deviceToken= " + deviceToken);
		tokens.add(deviceToken);
		return true;
	}

	// 给单个设备推送消息
	public boolean pushMessage(String pushcontent, String pushnumber,
			String pushsound, Map<String, Object> data) throws Exception {
		PushNotificationPayload pl = new PushNotificationPayload();
		if (null != pushcontent)
			pl.addAlert(pushcontent);
		if (null != pushnumber)
			pl.addBadge(Integer.parseInt(pushnumber));
		if (null != pushsound)
			pl.addSound(pushsound);
		if (null != data) {
			for (String key : data.keySet()) {
				pl.addCustomDictionary(key, data.get(key));
			}
		}

		AppleNotificationServer server = new AppleNotificationServerBasicImpl(
				certificatePath, certificatePassword, false);
		PushNotificationManager manager = new PushNotificationManager();
		manager.initializeConnection(server);

		Device device = new BasicDevice();
		device.setToken(tokens.get(0));
		PushedNotification notification = manager.sendNotification(device, pl,
				true);
		manager.stopConnection();
		System.out.println("notification:" + notification.toString());
		if (notification.isSuccessful()) {
			return true;
		} else {
			return false;
		}
	}

	// 一次给多个设备推送信息
	public boolean pushToEveryDeviceMessage(String pushcontent,
			String pushnumber, String pushsound, Map<String, Object> data)
			throws Exception {
		PushNotificationPayload pl = new PushNotificationPayload();
		if (null != pushcontent)
			pl.addAlert(pushcontent);
		if (null != pushnumber)
			pl.addBadge(Integer.parseInt(pushnumber));
		if (null != pushsound)
			pl.addSound(pushsound);
		if (null != data) {
			for (String key : data.keySet()) {
				pl.addCustomDictionary(key, data.get(key));
			}
		}

		AppleNotificationServer server = new AppleNotificationServerBasicImpl(
				certificatePath, certificatePassword, false);
		PushNotificationManager manager = new PushNotificationManager();
		manager.initializeConnection(server);

		List<Device> devicelist = new ArrayList<Device>();
		for (int i = 0; i < tokens.size(); i++) {
			Device device = new BasicDevice();
			device.setToken(tokens.get(i));
			if (i == 2) {// 将其中一个token故意写错的话，这个被写错token的设备将得不到推送消息
				device.setToken("wrong token");
			}
			devicelist.add(device);
		}
		PushedNotifications notifications = manager.sendNotifications(pl,
				devicelist);
		manager.stopConnection();

		List<PushedNotification> failedNotifications = notifications
				.getFailedNotifications();
		List<PushedNotification> successfulNotifications = notifications
				.getSuccessfulNotifications();
		int failed = failedNotifications.size();
		int successful = successfulNotifications.size();

		System.out.println("successfulcount=" + successful + "failedcount="
				+ failed);

		if (failed == 0) {
			return true;
		} else {
			return false;
		}
	}

	// 用Thread给多个设备推送消息，这里开启的是list模式，list模式将预先设置好需要推送的设备的device，然后当线程启动后，逐一的进行推送。
	public boolean pushToEveryDeviceUseThreadListMode(String pushcontent,
			String pushnumber, String pushsound, Map<String, Object> data)
			throws Exception {
		PushNotificationPayload pl = new PushNotificationPayload();
		if (null != pushcontent)
			pl.addAlert(pushcontent);
		if (null != pushnumber)
			pl.addBadge(Integer.parseInt(pushnumber));
		if (null != pushsound)
			pl.addSound(pushsound);
		if (null != data) {
			for (String key : data.keySet()) {
				pl.addCustomDictionary(key, data.get(key));
			}
		}

		AppleNotificationServer server = new AppleNotificationServerBasicImpl(
				certificatePath, certificatePassword, false);
		PushNotificationManager manager = new PushNotificationManager();
		manager.initializeConnection(server);

		List<Device> devicelist = new ArrayList<Device>();
		for (int i = 0; i < tokens.size(); i++) {
			Device device = new BasicDevice();
			device.setToken(tokens.get(i));
			devicelist.add(device);
		}

		NotificationThread thread = new NotificationThread(manager, server, pl,
				devicelist);
		System.out.println("maxconn="
				+ thread.getMaxNotificationsPerConnection());
		thread.setMaxNotificationsPerConnection(2);// 即使设置成2了，还是可以给大于2个的进行推送。
		System.out.println("maxconn2="
				+ thread.getMaxNotificationsPerConnection());
		thread.start();
		List<PushedNotification> successfulNotifications = thread
				.getSuccessfulNotifications();
		List<PushedNotification> failedNotifications = thread
				.getFailedNotifications();
		int failed = failedNotifications.size();
		int successful = successfulNotifications.size();
		System.out.println("successfulcount=" + successful + "failedcount="
				+ failed);

		if (failed == 0) {
			return true;
		} else {
			return false;
		}
	}

	// 利用queue模式进行推送
	public boolean pushToEveryDeviceUseThreadQueueMode(String pushcontent,
			String pushnumber, String pushsound, Map<String, Object> data)
			throws Exception {
		PushNotificationPayload pl = new PushNotificationPayload();
		if (null != pushcontent)
			pl.addAlert(pushcontent);
		if (null != pushnumber)
			pl.addBadge(Integer.parseInt(pushnumber));
		if (null != pushsound)
			pl.addSound(pushsound);
		if (null != data) {
			for (String key : data.keySet()) {
				pl.addCustomDictionary(key, data.get(key));
			}
		}

		queuethread.setMaxNotificationsPerConnection(20);

		Device device = new BasicDevice();
		device.setToken(tokens.get(queuecount++));
		queuethread.add(pl, device);

		List<PushedNotification> successfulNotifications = queuethread
				.getSuccessfulNotifications();
		List<PushedNotification> failedNotifications = queuethread
				.getFailedNotifications();
		int failed = failedNotifications.size();
		int successful = successfulNotifications.size();

		System.out.println("successfulcount=" + successful + "failedcount="
				+ failed);

		if (failed == 0) {
			return true;
		} else {
			return false;
		}
	}
}
